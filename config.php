<?php
class Dbh {

	//database connection
	private $servername;
	private $username;
	private $password;
	private $dbname;

	protected function connect(){
		$this->servername = "localhost";
		$this->username = "root";
		$this->password = "";
		$this->dbname = "catalog";

		$conn = new mysqli(
			$this->servername,
			$this->username,
			$this->password,
			$this->dbname
		);
		return $conn;
	}

}


