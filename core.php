<?php
include 'models.php';

/**
 * get product from function database
 */

	$items = new Items();
    $disk = $items->getDisc();
    $furn = $items->getFurnitura();
    $book = $items->getBook();

/**
 * if select button apply
 * Can switch to option add new product || delete product
 * if select new product redirect to add.php
 * if select mass delete - delete product in the select checkbox
 */

if(isset($_POST['apply'])) {
    if(isset($_POST['unit']) && $_POST['unit'] == 'delete') {
        if(isset($_POST[''])==false){
            header("Location: index.php");

        }

        $delete = new Delete();

        if(isset($_POST['diskId'])) {
        	$getId = $_POST['diskId'];
        foreach ($getId as $id) {
           $delete->deleteDisk($id);
               header("Location: index.php");
           }
        }

        if (isset($_POST['bookId'])) {
        	$bookId = $_POST['bookId'];
        foreach ($bookId as $id) {
            $delete->deleteBook($id);
                header("Location: index.php");
            }
        }
        if (isset($_POST['furId'])) {
        	$furId = $_POST['furId'];
        foreach ($furId as $id) {
            $delete->deleteFurn($id);
                header("Location: index.php");
            }
        }
    }else{
        header("Location: add.php");
    }
}

/**
 * if select button save
 * can change type to send new product in Database
 *
 */
if (isset($_POST['save'])) {

	$sku = $_POST['sku'];
	
 	$title = $_POST['title'];

 	$price = (float)$_POST['price'];

 	if(isset($_POST['sizes'])){

 		$sizes = $_POST['sizes'];
 	}

  	if(isset($_POST['weight'])){

 		$weight = $_POST['weight'];
 	}

  	if(isset($_POST['height'])){

 		$height = $_POST['height'];
 	}

  	if(isset($_POST['width'])){

 		$width = $_POST['width'];
 	}

  	if(isset($_POST['length'])){

 		$length = $_POST['length'];
 	}
 	
 	//insert 
 	$insert = new Insert();

    if(isset($_POST['typeSwitcher']) && $_POST['typeSwitcher'] == 'disk') {

    	$insert->addDisc($sku,$title,$price,$sizes); //$sku,$title,$price,$sizes
            header("Location: index.php");
    }

    if(isset($_POST['typeSwitcher']) && $_POST['typeSwitcher'] == 'book') {

        $insert->addBook($sku,$title,$price,$weight);
        	header("Location: index.php");

    }
    if(isset($_POST['typeSwitcher']) && $_POST['typeSwitcher'] == 'fur') {

    	$insert->addFurn($sku,$title,$price,$height,$width,$length);
        	header("Location: index.php");

    }else{
        header("Location: index.php");
    }
}

