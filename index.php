<?php
	
	include 'core.php';

?>

<!DOCTYPE html>
<html>
<head>
	<title>CATALOG</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
	<div class="wrapper">
    <div class="header">
        <a style="font-weight: bold;">Product list</a>
        <div class="optionBtn" >
       <select class="unitDiv" name="unit" form="showForm" >
           <option value="add" name="add" >Add new product</option>
           <option value="delete" name="delete" >Mass Delete Action</option>
       </select>
       <input id="apply" type="submit" name="apply" value="apply" form="showForm">
        </div> <!--optionBtn -->
    </div><!--header -->

      <div class="content" >
            <form id="showForm" method="POST" action="core.php">
                <table  cellpadding="0" cellspacing="0"  class="disk" border="0" >
                    <tr >
                        <?php foreach ($disk as $item):  // show array disk from database?>
                        <td valign="top"  >
                            <div class="diskDiv" >
                                <input type="checkbox" name="diskId[]" value="<?=$item['id']?>" style="float: left;">
                                <a ><?=$item['sku']?></a><br>
                                <a ><?=$item['title']?></a><br>
                                <a ><?=$item['price']?> $</a><br>
                                <a >Size: <?=$item['sizes']?> Mb</a><br>
                            </div>
                        </td>
                    <?php endforeach; ?>
                    </tr>
                </table>
            <table  cellpadding="0" cellspacing="0"  class="disk" border="0"  >
                <tr >
                    <?php foreach ($book as $item): // show array book from database?>
                        <td valign="top"  >
                            <div class="bookDiv" >
                                <input type="checkbox" name="bookId[]" value="<?=$item['id']?>" style="float: left;">
                                <a ><?=$item['sku']?></a><br>
                                <a ><?=$item['title']?></a><br>
                                <a ><?=$item['price']?> $</a><br>
                                <a >Weight: <?=$item['weight']?> Kg</a><br>
                            </div>
                        </td>
                    <?php endforeach; ?>
                </tr>
            </table>
            <table  cellpadding="0" cellspacing="0"  class="disk" border="0" style="text-align: center; margin-left:  10px;" >
                <tr >
                    <?php foreach ($furn as $item):// show array furniture from database?>
                        <td valign="top"  >
                            <div class="furnitureDiv" >
                                <input type="checkbox" name="furId[]" value="<?=$item['id']?>" style="float: left;">
                                <a ><?=$item['sku']?></a><br>
                                <a ><?=$item['title']?></a><br>
                                <a ><?=$item['price']?> $</a><br>
                                <a >Dimension: <?=$item['height']?>x<?=$item['width']?>x<?=$item['length']?></a><br>
                            </div>
                        </td>
                    <?php endforeach; ?>
                </tr>
            </table>
            </form>
            <div id="result"></div>
        </div><!--content -->

</body>
</html>