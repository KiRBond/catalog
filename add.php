<?php include 'core.php'; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product Add</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/myscript.js"></script>
   </head>
   <body>
   <div class="wrapper">
       <div class="header">
           <a style="font-weight: bold;">Product add</a>
               <input type="submit" name="save" value="save" form="addForm" style="float: right;" />
       </div>
           <div class="content" style="margin-left: 20px;">
               <form id="addForm" method="POST" action="core.php" >
                   <table>
                       <tr>
                           <td>SKU</td>
                           <td><input type="text" name="sku"  ></td><br>
                       </tr>
                       <tr>
                           <td>Name</td>
                           <td><input type="text" name="title" ></td>
                       </tr>
                       <tr>
                           <td>Price</td>
                           <td><input type="text" name="price" ></td>
                       </tr>
                   </table><br>
                   <label>Type Switcher</label>
                   <select id="type" name="typeSwitcher" onchange="populate('type','typeDiv')" >
                       <option>Select Type</option>
                       <option value="disk" >Dvd-disk</option>
                       <option value="book" >Book</option>
                       <option value="fur" >Furniture</option>
                   </select><br>
                    <br><div id="typeDiv">

                    </div>

               </form>
           </div><!--content -->
   </div><!--wrapper-->
   </body>
   </html>