<?php

include 'config.php';
/*
SELECT FROM database all items
*/
class Items extends Dbh {

/**
 * Get products from Database disk
*/

    public function getDisc(){
        $sql = "SELECT * FROM disc";
        $result = $this->connect()->query($sql);
        $numRows = $result->num_rows;
        if($numRows > 0){
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }

    }

/**
* Get products from Database book
*/

    public function getBook(){
        $sql = "SELECT * FROM book";
        $result = $this->connect()->query($sql);
        $numRows = $result->num_rows;
        if($numRows > 0){
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }

    }
/**
* Get products from Database furniture
*/

    public function getFurnitura(){
        $sql = "SELECT * FROM furniture";
        $result = $this->connect()->query($sql);
        $numRows = $result->num_rows;
        if($numRows > 0){
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }

    }

}

/*
Delete items FROM database by id
*/
class Delete extends Dbh {


    public function deleteDisk($id){

        $sql = "DELETE FROM disc WHERE id = " . $id . "";
        $this->connect()->query($sql);

    }

    public function deleteBook($id){

        $sql = "DELETE FROM book WHERE id = " . $id . "";
        $this->connect()->query($sql);
    }

    public function deleteFurn($id){

        $sql = "DELETE FROM furniture WHERE id = " . $id . "";
        $this->connect()->query($sql);
    }

}


/**
* insert in database items
*/
class Insert extends Dbh
{

    public function addDisc($sku,$title,$price,$sizes){

        $sql = "INSERT INTO disc (sku, title, price, sizes) 
            VALUES ('".addslashes($sku)."',
            '".addslashes($title)."',
            '".addslashes($price)."',
            '".addslashes($sizes)."')";

        $this->connect()->query($sql);

    }

    public function addBook($sku,$title,$price,$weight){
        $sql = "INSERT INTO book (sku, title, price, weight) 
            VALUES ('".addslashes($sku)."',
            '".addslashes($title)."',
            '".addslashes($price)."',
            '".addslashes($weight)."')";
            $this->connect()->query($sql);
    }

    public function addFurn($sku,$title,$price,$height,$width,$length){

        $sql = "INSERT INTO furniture (sku, title, price, height, width, length) 
            VALUES ('".addslashes($sku)."',
            '".addslashes($title)."',
            '".addslashes($price)."',
            '".addslashes($height)."',
            '".addslashes($width)."',
            '".addslashes($length)."')";
            $this->connect()->query($sql);
        
    }


}
